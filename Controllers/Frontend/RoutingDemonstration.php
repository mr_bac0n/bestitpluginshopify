<?php
use \BestItPluginShopify\api_connector\ApiClient;
class Shopware_Controllers_Frontend_RoutingDemonstration extends Enlight_Controller_Action
{
    /** Beispielcontroller
     *
     *  Nötig falls man Templates verwenden möchte (wir wahrscheinlich nicht)
     *
     *  preDispatch() bevor die Seite aufgerufen wird, wird das Template unter Resources/views gesucht
     *
     *
     *
     *
     */
    public function indexAction() {

        //API Testausgabe

        $apitest = new ApiClient(
        //URL of shopware REST server
            'http://localhost/shopware/api',
            //Username
            'admin',
            //User's API-Key
            'WeHgWaWQDmTRrAyktWJfLDTPGEDc8R8MMtLiICZx'
        );

        $decodedResult = $apitest->get('categories');
        $this->view->assign('apitest', $decodedResult);
    }



}