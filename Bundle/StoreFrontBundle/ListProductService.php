<?php


namespace BestItPluginShopify\Bundle\StoreFrontBundle;

use Shopware\Bundle\StoreFrontBundle\Service\ListProductServiceInterface;
use Shopware\Bundle\StoreFrontBundle\Struct\ProductContextInterface;
use Shopware\Bundle\StoreFrontBundle\Struct\ListProduct;

class ListProductService implements ListProductServiceInterface
{
    public function __construct()
    {

    }

    public function getList(array $numbers, ProductContextInterface $context)
    {
        // $products = $this->originalService->getList($numbers, $context);

        // API-Credentials festlegen
        $api = '130f6363063532279211bb3c18a8ddd0';
        $password = '2eef6330f90dc1af51e036276ead7490';
        $url = 'w-hs-dev-store.myshopify.com';

        // API-Credentials
        $sUrl           = 'https://'.$api.':'.$password.'@'.$url.'/admin/products.json';
        $curl   = curl_init();
        curl_setopt($curl, CURLOPT_URL, $sUrl);
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

        // URL aufrufen (=API-Call durchführen) und das Ergebnis in $result speichern
        $result   = curl_exec($curl);
        curl_close($curl);

        // Aus dem Body der HTTP-Response ($result) die Daten decodieren und in ein
        $result = json_decode($result, true);
        $result = $result['products'];

        $products = [];

        $i = $j = 1;
        foreach($result as $product)
        {
            $oProduct =  new ListProduct($product['id'], $i, $j);
            $oProduct->setName($product['title']);
            $oProduct->setStock(999);
            $oProduct->setShortDescription($product['body_html']);
            $oProduct->setLongDescription($product['body_html']);
            $oProduct->setReleaseDate( new \DateTime());
            $oProduct->setShippingTime(1);
            $oProduct->setShippingFree(true);
            $oProduct->setCloseouts(false);
            //$oProduct->hasProperties(true);
            $oProduct->setCreatedAt(new \DateTime());
            $oProduct->setKeywords(explode(', ', $product['tags']));
            $oProduct->setMetaTitle($product['title']);
            $oProduct->setAllowsNotification(false);
            //$oProduct->setCover();

            $products[] = $oProduct;

            $i++;
            $j++;
        }



        //exit(print_r($products[0]->getName()));

        return $products;
    }

    public function get($number, ProductContextInterface $context)
    {
        exit("kaputt");
        return $redisProduct;
}
}